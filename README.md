# travail-a-distance

Ce dépôt git a pour objectif de centraliser un ensemble ressources
destinées à l'utilisation de divers outils d'enseignement à
distance, à Polytech'Lille :

* `outils.md` une brève synthèse des outils notoires pour le travail à
  distance, par Julien Forget ;

* `discord.md` une introduction à Discord, par Boris Jacquot ;

* `list_machines.sh` un script pour lister les machines de TP allumées,
  par Xavier Redon et Andrei Floréa.
